<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="CSS/estilo.css" type="text/css">
    </head>
    <body>
        <h1>PRÁCTICA CREACIÓN DE UN BLOG</h1>
        <a href="index.php">Inicio</a><a href="AcercaDe.php">Acerca de</a>
        <a href="Enlaces.php">Enlaces de interes</a><br/><br/>     

        <form name="loginform" method="post" action="ComprobarLogin.php">
            <fieldset>
                <legend>Inicio de sesión</legend>
                <table>
                    <tr>
                        <td>
                            <label for="login">Login</label>
                        </td>
                        <td>
                            <input type="text" name="userName" id="login" value="" size="8"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="pass">Password</label>
                        </td>
                        <td>
                            <input type="password" name="password" id="pass"  size="8" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <input type="submit" name="enviar" id="enviar" value="Entrar" />
                        </td>

                    </tr>
                </table>


            </fieldset>
            <?php
            if (isset($_GET["error"]) && $_GET["error"] == 1) {
                echo "<p class=\"error\">Login incorrecto.</p>";
            }
            ?>
        </form>
    </body>
</html>
