<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

//Abre la conexión al SGBD
if (!($enlace = @mysql_connect("localhost", "root", "") )) {
    echo "No se pudo conectar";
}

if (!isset($_SESSION["nombre"])) {
    header("Location: login.php");
}

//Selecciona la base de datos a utilizar
if (!mysql_select_db("Blog")) {
    echo "No se pudo seleccionar la BBDD";
}

if (isset($_GET["id"])) {
    $sql = "select post.title, post.description, post.idPost from post where idPost=" . $_GET["id"];
    $resultado = mysql_query($sql);
    $registro = mysql_fetch_assoc($resultado);
}

if (isset($_POST["postTitle"]) && isset($_POST["postText"])) {
    $sql = "UPDATE post SET title='" . $_POST["postTitle"] . "',description='" . $_POST["postText"] . "'"
            . " WHERE idPost=" . $_POST["idPost"];

    $resultado = mysql_query($sql) or die(mysql_error());
    header("Location: AdministrarBlog.php");
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Blog Desarrollo web</title>
        <link rel="stylesheet" href="CSS/estilo.css" type="text/css">
    </head>
    <body>
        <a id="loginButton" href="LogOut.php">LOGOUT</a>
        <h1>PRÁCTICA CREACIÓN DE UN BLOG</h1>
        <a href="AdministrarBlog.php">Modificar post</a>
        <a href="ModificarPerfil.php">Editar perfil</a>
        <a href="administrarLinks.php">Administrar links</a>
        <a href="PostTitulo.php">Buscar Post</a><br/><br/>

        <form id="createpostform" method="post" action="ModificarPost.php">

            <h4>TITULO DEL POST</h4>
            <input type="text" name="postTitle"   size="80" value="<?php echo $registro["title"]; ?>" />

            <h4>CONTENIDO DEL POST</h4>
            <textarea rows=5 cols=60 name="postText"><?php echo $registro["description"]; ?></textarea><br/><br/>

            <input type="submit" name="enviar" id="enviar" value="Modificar post" />
            <input type="hidden" name="idPost" value="<?php echo $registro["idPost"]; ?>"/>
        </form>
    </body>
</html>
