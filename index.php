
<?php
//Establecer codificación UTF-8 para mostrar correctamente caracteres como las
//tildes, eñes, etc.
header('Content-Type: text/html; charset=UTF-8');

//Se necesita para meter leer las variables de sesión
session_start();

//Abre la conexión al SGBD
@mysql_connect("localhost", "root", "");


//Selecciona la base de datos a utilizar
if (!mysql_select_db("Blog")) {
    echo "No se pudo seleccionar la BBDD";
}

//Listamos todos los post que haya en la base de datos
$sql = "select post.title, post.description from post order by fecha desc";


//Se ejecuta la query
$resultado = mysql_query($sql);
$losPosts = "<div id=\"losPosts \">";

while ($registro = mysql_fetch_assoc($resultado)) {
    $losPosts .= "<div>";
    $losPosts .= '<span class="tituloPost">' . $registro["title"] . " </span><br/> " . $registro["description"];
    $losPosts .= "</div>";
}
$losPosts .= "</div>";
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Blog Desarrollo web</title>
        <link rel="stylesheet" href="CSS/estilo.css" type="text/css">
    </head>
    <body>
        <a id="loginButton" href="Login.php">Login</a>
        <h1>PRÁCTICA CREACIÓN DE UN BLOG</h1>
        <a href="index.php">Inicio</a><a href="AcercaDe.php">Acerca de</a>
        <a href="Enlaces.php">Enlaces de interes</a><br/><br/>

        <div id="losPosts" >
            <?php
            echo $losPosts;
            ?>
        </div>    
    </body>
</html>
