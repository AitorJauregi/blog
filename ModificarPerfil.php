<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location: login.php");
}

//Abre la conexión al SGBD
if (!($enlace = @mysql_connect("localhost", "root", "") )) {
    echo "No se pudo conectar";
}

//Selecciona la base de datos a utilizar
if (!mysql_select_db("Blog")) {
    echo "No se pudo seleccionar la BBDD";
}

$sql = "select users.name, users.lastName, users.userName, users.password, users.avatar, users.info from users";
$resultado = mysql_query($sql);
$registro = mysql_fetch_assoc($resultado);


if (isset($_POST["name"]) && isset($_POST["lastName"]) && isset($_POST["userName"]) &&
        isset($_POST["password"]) && isset($_POST["photo"]) && isset($_POST["info"])) {

    $sql = "UPDATE users SET name='" . $_POST["name"] . "', lastName='" . $_POST["lastName"] . "',userName='" . $_POST["userName"] . "',password=sha1('" . $_POST["password"] . "'),avatar='" . $_POST["photo"] . "', info='" . $_POST["info"] . "'";

    mysql_query($sql) or die(mysql_error());

    $sql = "select users.name, users.lastName, users.userName, users.password, users.avatar, users.info from users";
    $resultado = mysql_query($sql);
    $registro = mysql_fetch_assoc($resultado);
    header("Location: AdministrarBlog.php");
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PRÁCTICA CREACIÓN DE UN BLOG</title>
        <link rel="stylesheet" href="CSS/estilo.css" type="text/css">
    </head>
    <body>
        <a id="loginButton" href="LogOut.php">Logout</a>
        <h1>PRÁCTICA CREACIÓN DE UN BLOG</h1>
        <a href="AdministrarBlog.php">Modificar post</a>
        <a href="ModificarPerfil.php">Editar perfil</a>
        <a href="administrarLinks.php">Administrar links</a>
        <a href="PostTitulo.php">Buscar Post</a><br/><br/>
        <div id="izquierda">
            <form id="perfilform" method="POST" action="ModificarPerfil.php">
                <fieldset id="perfil">     
                    <legend>Modificar perfil</legend>

                    <label for="name">Nombre:</label>
                    <input type="text" id="" name="name"  value="<?php echo $registro["name"]; ?>" size="50"/><br />

                    <label for="lastName">Apellidos:</label>
                    <input type="text" id="" name="lastName"  value="<?php echo $registro["lastName"]; ?>" size="50"/><br />

                    <label for="userName">Nombre de usuario:</label>
                    <input type="text" id="" name="userName"  value="<?php echo $registro["userName"]; ?>" size="50"/><br />

                    <label for="password">Contraseña:</label>
                    <input type="text" id="" name="password"  value="<?php echo $registro["password"]; ?>" size="30"/><br />

                    <label for="photo">Foto:</label>
                    <input type="text" id="" name="photo"  value="<?php echo $registro["avatar"]; ?>"/><br />

                    <label for="info">Información:</label>
                    <input type="text" id="" name="info"  value="<?php echo $registro["info"]; ?>"/><br />

                    <input type="submit" value="Enviar"/>
                </fieldset>
            </form> 
        </div>
    </body>
</html>
